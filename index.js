import {AppRegistry, Modal, SafeAreaView, StatusBar, View} from 'react-native';
import {Icon} from 'native-base';
import App from './App';
import {heightPercentageToDP as hp, widthPercentageToDP as wp,} from 'react-native-responsive-screen';
import {name as appName} from './app.json';
import React, {Component} from 'react';
import {applyMiddleware, createStore} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import Reducers from './src/reducer/MainReducer';
import NetInfo from '@react-native-community/netinfo';
import Colors from "./src/utils/Colors";
import PText from "./src/utils/PText";

console.disableYellowBox = true; //to disable warnings

const store = createStore(Reducers, applyMiddleware(thunk));

class MainApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            noInternet: false,
        };
    }

    componentWillMount() {
        NetInfo.configure({
            reachabilityUrl: 'https://clients3.google.com/generate_204',
            reachabilityTest: async response => response.status === 204,
            reachabilityLongTimeout: 60 * 1000, // 60s
            reachabilityShortTimeout: 5 * 1000, // 5s
            reachabilityRequestTimeout: 15 * 1000, // 15s
        });
        // Subscribe
        const unsubscribe = NetInfo.addEventListener(state => {
            try {
                var noInternet = false;
                if (!state.isInternetReachable && state.isInternetReachable != null) {
                    noInternet = true
                }
                this.setState({
                    noInternet: noInternet,
                });
            } catch (e) {
            }
        });
    }

    render() {
        return (
            <Provider store={store}>
                <StatusBar translucent backgroundColor={Colors.PrimaryColor}/>
                <SafeAreaView style={{flex: 1}}>
                    <App/>
                </SafeAreaView>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.noInternet}>
                    <View
                        style={{
                            backgroundColor: '#8c8b8beb',
                            width: wp('100%'),
                            height: hp('100%'),
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                        <View
                            style={{
                                backgroundColor: '#FFF',
                                width: wp('70%'),
                                height: hp('50%'),
                                shadowColor: 'grey',
                                shadowOffset: {
                                    width: 0,
                                    height: 1,
                                },
                                shadowOpacity: 0.22,
                                shadowRadius: 1.5,
                                elevation: 5,
                            }}>
                            <View
                                style={{
                                    height: wp('50%'),
                                    width: wp('70%'),
                                    backgroundColor: Colors.PrimaryColor,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                <Icon
                                    type={'MaterialIcons'}
                                    name="cloud-off"
                                    style={{color: '#FFF', fontSize: wp("30%")}}
                                />
                            </View>
                            <View
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    flex: 1,
                                }}>
                                <PText style={{fontSize: wp("4")}} text={'Poor network connection detected'}></PText>
                                <PText style={{fontSize: wp("4")}} text={"Please check your connectivity"}></PText>
                            </View>
                        </View>
                    </View>
                </Modal>
            </Provider>
        );
    }
}

AppRegistry.registerComponent(appName, () => MainApp);
