import {
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { createDrawerNavigator } from "react-navigation-drawer";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import SplashScreen from "./src/components/Splash";
import DashBoardScreen from "./src/components/Dashboard";
import SideMenu from "./src/components/SideMenu";
import LoginScreen from "./src/components/LoginScreen";
const MainNavigator = createDrawerNavigator(
  {
    DashBoard: {screen: DashBoardScreen},
  },
  {
    headerMode: 'none',
    initialRouteName: 'DashBoard',
    contentComponent: SideMenu,
    drawerWidth: wp("70%"),
  },
);
const StartNavigator = createStackNavigator(
  {
    Splash: { screen: SplashScreen },
    LoginScreen: { screen: LoginScreen },
    DashboardView: {screen: MainNavigator},
  },
  {headerMode: 'none', initialRouteName: 'Splash'},
);
const App = createAppContainer(StartNavigator);
export default App;
