import {
    FETCH_TOPRATEDRECORD_FAILURE,
    FETCH_TOPRATEDRECORD_REQUEST,
    FETCH_TOPRATEDRECORD_SUCCESS,
    FETCH_RECORD_SEARCH,
    FETCH_TRENDING_REQUEST, FETCH_TRENDING_FAILURE, FETCH_TRENDING_SUCCESS,
    FETCH_DISCOVER_REQUEST, FETCH_DISCOVER_FAILURE, FETCH_DISCOVER_SUCCESS,
} from '../Actiontypes'

export const fetchRequest = () => ({
    type: FETCH_TOPRATEDRECORD_REQUEST
})

export const fetchFailure = error => ({
    type: FETCH_TOPRATEDRECORD_FAILURE,
    payload: error
})

export const fetchSuccess = json => ({
    type: FETCH_TOPRATEDRECORD_SUCCESS,
    payload: json
})

export const search = (msg, type) => ({
    type: FETCH_RECORD_SEARCH,
    search: msg,
    searchType: type
})

export const fetchTrendingRequest = () => ({
    type: FETCH_TRENDING_REQUEST
})

export const fetchTrendingFailure = error => ({
    type: FETCH_TRENDING_FAILURE,
    payload: error
})

export const fetchTrendingSuccess = json => ({
    type: FETCH_TRENDING_SUCCESS,
    payload: json
})

export const fetchDiscoverRequest = () => ({
    type: FETCH_DISCOVER_REQUEST
})

export const fetchDiscoverFailure = error => ({
    type: FETCH_DISCOVER_FAILURE,
    payload: error
})

export const fetchDiscoverSuccess = json => ({
    type: FETCH_DISCOVER_SUCCESS,
    payload: json
})
export const fetchTopRatedRecord = (url) => {
    return async dispatch => {
        dispatch(fetchRequest);
        try {
            return fetch(url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .then((response) => response.json())
                .then((response) => {
                    dispatch(fetchSuccess(response.results))
                })
                .catch(error => dispatch(fetchFailure(error.data)));
        } catch (error) {
            dispatch(fetchFailure(error))
            console.log("error", error)
        }
    }
}

export const fetchTrendingRecord = (url) => {
    return async dispatch => {
        dispatch(fetchTrendingRequest);
        try {

                    console.log("response", url)
            return fetch(url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .then((response) => response.json())
                .then((response) => {
                    dispatch(fetchTrendingSuccess(response.results))
                })
                .catch(error => dispatch(fetchTrendingFailure(error.data)));
        } catch (error) {
            dispatch(fetchTrendingFailure(error))
            console.log("error", error)
        }
    }
}

export const fetchDiscoverRecord = (url) => {
    return async dispatch => {
        dispatch(fetchDiscoverRequest);
        try {

                    console.log("response", url)
            return fetch(url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .then((response) => response.json())
                .then((response) => {
                    dispatch(fetchDiscoverSuccess(response.results))
                })
                .catch(error => dispatch(fetchDiscoverFailure(error.data)));
        } catch (error) {
            dispatch(fetchDiscoverFailure(error))
            console.log("error", error)
        }
    }
}
