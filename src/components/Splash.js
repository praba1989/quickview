import React from 'react';
import {listenOrientationChange as lor, widthPercentageToDP as wp,} from 'react-native-responsive-screen';
import {AsyncStorage, Image, ImageBackground, StyleSheet, Text, View,} from 'react-native';
import PText from "../utils/PText";
import Colors from "../utils/Colors";

export default class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    async componentDidMount() {
        this.timeoutHandle = setTimeout(async () => {
            this.props.navigation.navigate("LoginScreen")
        }, 4000);
        lor();
    }

    componentWillUnmount() {
        clearTimeout(this.timeoutHandle);
    }

    render() {
        return (
            <View style={styles.container}>
                <PText style={styles.textStyle}
                       text={"Splash"}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',

    },
    textStyle: {
        color: Colors.PrimaryColor,
        textAlign: "center",
        fontSize: wp("8%"),
        fontWeight: 'bold',
        fontStyle: 'italic',
    }
});
