import React from 'react';
import {Container, Icon, View} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {heightPercentageToDP as hp, widthPercentageToDP as wp,} from 'react-native-responsive-screen';
import {
    AsyncStorage,
    FlatList,
    Image,
    ImageBackground,
    ScrollView,
    StatusBar,
    StyleSheet,
    TextInput,
    TouchableOpacity
} from 'react-native';
import Colors from "../utils/Colors";
import ListScreen from "./ListScreen";
import TrendingScreen from "./TrendingScreen";
import DiscoverScreen from "./DiscoverScreen";
import {NavigationActions, StackActions} from "react-navigation";

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentVisibleIndex: 0,
            mainBoard: true,
            entryScreen: false,
            serviceScreen: false,
            circleScreen: false,
            slateScreen: false,
            searchActive: false,
            filterSelectedIndex: 0
        };

    }



    render() {
        return <Container style={{marginTop: StatusBar.currentHeight}}>

            {this.state.mainBoard && <ListScreen navigation={this.props.navigation}/>}
            {this.state.entryScreen && <TrendingScreen/>}
            {this.state.serviceScreen && <DiscoverScreen/>}


            <View style={{
                height: wp('1%'),
                backgroundColor: "grey",
                opacity: 0.1,
            }}>
            </View>
            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: wp('4%'),
                backgroundColor: "white",
                paddingHorizontal: wp("10%"),
                opacity: 1,
                borderColor: Colors.LineColor,
            }}>

                <TouchableOpacity
                    onPress={() => {
                        this.setState({
                            mainBoard: true,
                            entryScreen: false,
                            serviceScreen: false,
                        })
                    }}>

                    <Icon
                        style={[styles.iconStyle, {color: this.state.mainBoard ? Colors.PrimaryColor : Colors.SecondaryColor}]}
                        type="FontAwesome"
                        name="user"
                    />
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => {
                        this.setState({
                            mainBoard: false,
                            entryScreen: true,
                            serviceScreen: false,
                        })
                    }}>
                    <Icon
                        style={[styles.iconStyle, {color: this.state.entryScreen ? Colors.PrimaryColor : Colors.SecondaryColor}]}
                        type="Fontisto"
                        name="arrow-swap"
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.setState({
                            mainBoard: false,
                            entryScreen: false,
                            serviceScreen: true,
                        })
                    }}>
                    <Icon
                        style={[styles.iconStyle, {color: this.state.serviceScreen ? Colors.PrimaryColor : Colors.SecondaryColor}]}
                        type="FontAwesome"
                        name="file-text"
                    />
                </TouchableOpacity>
            </View>
        </Container>
    }
}

const styles = StyleSheet.create({
    bottomBarView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: wp('15%')
    }, searchSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        width: wp("75%"),
        height: hp('5%'),
        paddingRight: 10,
        paddingLeft: 10,
        color: "white",
    },
    iconStyle: {
        fontSize: wp('7%'),
        textAlign: 'center',
        color: "white",
    },
    topBoxView: {
        borderColor: Colors.SecondaryColor,
        borderWidth: 0,
        flexDirection: 'row',
        borderBottomWidth: 0,
        padding: wp('1%')
    },
    bottomBoxView: {
        flexDirection: 'row',
        justifyContent: 'center',
        width: wp('20%'),
        alignItems: 'center'
    },
    textStyle: {
        color: 'white',
        fontWeight: 'bold',
    },
    itemMainView: {
        flexDirection: 'row',
        position: 'absolute',
        backgroundColor: 'grey',
        opacity: 0.8,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Dashboard);
