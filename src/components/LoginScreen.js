import React from "react";
import {Container, Icon, Button} from "native-base";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {
    heightPercentageToDP as hp,
    listenOrientationChange as lor,
    removeOrientationListener as rol,
    widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import {Image, StyleSheet, TextInput, TouchableOpacity, View,} from "react-native";
import PText from "../utils/PText";
import Colors from "../utils/Colors";
import Toast from 'react-native-simple-toast'

class LoginScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            emailPno: "",
            password: "",
        };
    }

    async componentDidMount() {
        lor();
    }

    componentWillUnmount() {
        rol();
    }

    render() {
        return (
            <Container style={styles.container}>
                <View style={{marginTop: hp('10%')}}>
                    <Image
                        source={require("../assets/logo.png")}
                        style={styles.logoView}
                    />
                </View>
                <View style={styles.content}>
                    <View style={styles.inputView}>
                        <View style={styles.inputLabel}>
                            <PText text="Check In"
                                   style={{fontSize: wp('4.5%'), fontWeight: 'bold', marginVertical: hp('2.5%'),}}/>
                        </View>
                        <View style={styles.inputLabel}>
                            <PText text="Email /Mobile No" style={[styles.text, {color: this.state.emailPno =="" ? "grey": Colors.PrimaryColor}]}/>
                        </View>
                        <TextInput
                            style={[styles.input, {borderColor: this.state.emailPno =="" ? "grey": Colors.PrimaryColor}]}
                            onChangeText={(emailPno) => {
                                this.setState({emailPno});
                            }}
                            underlineColorAndroid="transparent"
                        />
                    </View>
                    <View style={styles.inputView}>
                        <View style={styles.inputLabel}>
                            <PText text="Password" style={[styles.text, {color: this.state.password =="" ? "grey": Colors.PrimaryColor}]}/>
                        </View>
                        <View style={[styles.passwordInputView, {borderColor: this.state.password =="" ? "grey": Colors.PrimaryColor}]}>
                        <TextInput
                            style={styles.passwordInput}
                            onChangeText={(password) => {
                                this.setState({password});
                            }}
                            underlineColorAndroid="transparent"
                        />

                            <Icon style={styles.iconStyle} type="FontAwesome" name="eye"/>

                        </View>
                    </View>
                    <View style={{alignSelf: "flex-end", margin: wp("2%")}}>
                        <TouchableOpacity
                        onPress={()=>{
                        }}>
                        <PText text="Forgotten Password ?" style={styles.text}/>
                        </TouchableOpacity>
                    </View>
                    <Button style={styles.button} onPress={()=>{

                        if(this.state.emailPno == ""){
                            Toast.show("Please enter Email/Phone number")
                            return;
                        }else if(this.state.password == ""){
                            Toast.show("Please enter Password")
                            return;
                        }
                        this.props.navigation.navigate("DashBoard")
                    }}>
                        <PText text="CHECK IN" style={{color: "white", fontWeight: 'bold'}}/>
                    </Button>
                    <View
                        style={{
                            marginTop: hp("3%"),
                            alignSelf: "center",
                            flexDirection: "row",
                        }}
                    >
                        <PText text="New User ? " style={styles.text}/>
                        <TouchableOpacity onPress={()=>{
                        }}>
                        <PText
                            text=" Register Here "
                            style={[styles.text, {fontWeight: "bold", color: Colors.PrimaryColor}]}
                        />
                        </TouchableOpacity>
                    </View>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        backgroundColor: 'white'
    },
    content: {
        marginTop: hp("0%"),
    },
    logoView: {
        width: wp("30%"),
        height: wp("30%"),
        resizeMode: "stretch",
        alignSelf: "center",
    },
    inputView: {
        marginVertical: hp("0.3%"),
    },
    input: {
        width: wp("75%"),
        backgroundColor: "#fff",
        borderWidth: 1,
        borderRadius: wp('1.5%'),
        borderColor: "grey",
        fontSize: wp("3%"),
        height: hp("5.5%"),
        paddingLeft: wp('2%')
    },
    passwordInputView: {
        flexDirection: 'row',
        width: wp("75%"),
        backgroundColor: "#fff",
        color: "grey",
        borderRadius: wp('1.5%'),
        borderWidth: 1,
        borderColor: "grey",
        fontSize: wp("3%"),
        height: hp("5.5%"),
        justifyContent: 'center',
        alignItems: 'center'
    },
    passwordInput: {
        width: wp("68%"),
        backgroundColor: "#fff",
        fontSize: wp("3%"),
    },
    text: {
        fontSize: wp('3'),
        marginVertical: hp('1%'),
    },
    inputLabel: {},
    button: {
        width: wp("75%"),
        alignSelf: "flex-end",
        marginTop: hp("2%"),
        justifyContent: "center",
        alignItems: "center",
        borderWidth: 0,
        padding: wp("3%"),
        backgroundColor: Colors.PrimaryColor,
        shadowColor: Colors.PrimaryColor,
        shadowOpacity: 1,
        elevation: 10,
        // shadowRadius: 50,
        borderColor: Colors.PrimaryColor,
        shadowOffset: {width: 10, height: 13},
        borderRadius: wp('1.5%'),
    },
    iconStyle: {
        fontSize: wp('4%'),
        padding: wp('0%'),
        color: Colors.SecondaryColor,
        fontWeight: 'bold'
    },
});

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({});
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
