import React from 'react';
import {Container, Icon} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    heightPercentageToDP as hp,
    removeOrientationListener as rol,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Alert, Image, StatusBar, StyleSheet, TouchableOpacity, View} from 'react-native';
import PText from "../utils/PText";
import Colors from "../utils/Colors";

class SideMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    async componentDidMount() {
    }

    componentWillUnmount() {
        rol();
    }

    logout(){
        Alert.alert(
            `Are you sure `,
            `Do you really want to logout?`,
            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'Yes',
                    onPress: () => {
                        this.props.navigation.closeDrawer();
                       console.log('Yes Pressed')
                    }
                },
            ],
            {cancelable: false})
    }

    render() {
        return <Container style={{padding: wp('0%'), backgroundColor: "white", marginTop: StatusBar.currentHeight,}}>

            <View style={{
                alignItems: 'center',
                padding: hp('1%'),
                flexDirection: 'row',
                borderBottomWidth: 0.5,
                borderColor: Colors.LineColor
            }}>
                {/*<View style={{borderWidth:wp("5%"), borderRadius: wp('10%'),width: wp('13%'), height: wp('13%'), }}>
            <Image style={{width: wp('13%'), height: wp('13%'), borderRadius: wp('10%')}} source={{uri: "https://www.gstatic.com/webp/gallery/4.jpg"}}/>
        </View>*/}
                <View
                    style={{
                        flexDirection: 'row',
                        marginVertical: hp('2%'),
                    }}>
                    <View
                        style={{
                            borderRadius: 100,
                            width: wp('16%'),
                            height: wp('16%'),
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: 'grey'
                        }}>
                        <Image
                            style={{
                                borderRadius: 100,
                                borderWidth: 1,
                                width: wp('14%'),
                                height: wp('14%'),
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                            rezizeMode={'cover'}
                            source={{uri: "https://www.gstatic.com/webp/gallery/4.jpg"}}
                        />
                    </View>
                </View>
                <View style={{paddingLeft: wp("1%")}}>
                    <PText style={{fontWeight: 'bold', fontSize: wp("3.5%")}} text={"John"}/>
                    <PText
                        style={{width: wp("55%"), fontWeight: 'bold', color: Colors.SecondaryColor, fontSize: wp("3%")}}
                        text={"email@gmail.com"}/>
                </View>
            </View>
            <View style={{marginTop: hp("3%")}}>
                <TouchableOpacity onPress={() => {
                    this.props.navigation.closeDrawer();
                }}>

                    <View style={styles.itemMainView}>

                        <Icon style={[styles.iconStyle, {}]} name="settings" type={"Feather"}/>
                        <PText style={{paddingLeft: wp('3%'),}} text={"Settings"}/>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    this.props.navigation.closeDrawer();
                }}>

                    <View style={styles.itemMainView}>

                        <Icon style={[styles.iconStyle, {}]} name="bars" type={"AntDesign"}/>
                        <PText style={{paddingLeft: wp('3%'),}} text={"Terms and Condition"}/>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    this.props.navigation.closeDrawer();
                }}>

                    <View style={styles.itemMainView}>

                        <Icon style={[styles.iconStyle, {}]} name="lock" type={"Entypo"}/>
                        <PText style={{paddingLeft: wp('3%'),}} text={"Privacy Policy"}/>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {

                    this.logout()
                }}>
                <View style={styles.itemMainView}>

                    <Icon style={[styles.iconStyle, {paddingLeft: wp('1%')}]} name="sign-out" type={"FontAwesome"}/>
                    <PText style={{paddingLeft: wp('3%'),}} text={"Log Out"}/>
                </View>
                </TouchableOpacity>
            </View>
        </Container>;
    }
}

const styles = StyleSheet.create({
    iconStyle: {
        fontSize: wp('6%'),
        textAlign: 'center',
        fontWeight: 'bold'
    },
    itemMainView: {
        flexDirection: 'row',
        padding: wp('2.5%'),
        alignItems: 'center',
    }
});

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({});
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SideMenu);
