import React from 'react';
import {Card, Container, Icon, View} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    heightPercentageToDP as hp,
    removeOrientationListener as rol,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {
    FlatList,
    Image,
    ImageBackground,
    ScrollView,
    StatusBar,
    StyleSheet,
    TextInput,
    TouchableOpacity
} from 'react-native';
import PText from "../utils/PText";
import Colors from "../utils/Colors";
import {Constant} from "../utils/Constant";
import {fetchDiscoverRecord} from "../action/ApiActions";
import Spinner from "react-native-loading-spinner-overlay";
import TopToolBar from "../utils/TopToolBar";

const ISO6391 = require('iso-639-1');

class DiscoverScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };

    }

    componentDidMount() {

        var api_url = Constant.base_url + "discover/tv?api_key=" + Constant.token;
        this.props.fetchDiscoverRecord(api_url);
    }

    renderItem(item, index) {


        return (
            <Card style={{
                borderRadius: wp("2%"),
                width: wp("98%"),
                height: hp("15%"),
                justifyContent: "center",
                alignItems: "center"
            }}>

                <ImageBackground style={{width: wp("98%"), height: hp("15%"), opacity: 0.5, borderRadius: wp("2%")}}
                                 source={{uri: "http://image.tmdb.org/t/p/w500" + item.poster_path}}>
                </ImageBackground>
                <View style={styles.itemMainView}>
                    <View>
                        <Image style={{width: wp("30%"), height: hp("15%"), borderRadius: wp("2%")}}
                               source={{uri: "http://image.tmdb.org/t/p/w500" + item.backdrop_path}}/>
                    </View>
                    <View style={{
                        width: wp("68"),
                        paddingLeft: wp("2%")
                    }}>
                        <PText style={[styles.textStyle, {color: 'black'}]} text={item.name}/>
                        <PText numberOfLines={2} style={[styles.textStyle, {fontSize: wp("3%")}]} text={item.overview}/>


                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon style={[styles.iconStyle, {fontSize: wp("2%")}]} type={"FontAwesome"}
                                  name={"language"}/>
                            <PText style={[styles.textStyle, {paddingLeft: wp("2%"), fontSize: wp("2%")}]}
                                   text={ISO6391.getName(item.original_language)}/>
                        </View>
                        <PText style={[styles.textStyle, {
                            paddingTop: hp("0%"),
                            textAlign: "right",
                            paddingRight: wp("1%"),
                            fontSize: wp("2.7%"),
                        }]} text={"Ratings " + item.vote_average + "/10"}/>

                    </View>
                </View>
            </Card>
        )
    }


    render() {
        return <Container>

            <View>
            <TopToolBar title={"Discover"}/>
            <ScrollView style={{minHeight: hp("80%"), marginBottom: hp("7%")}}>
                <View>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.props.record}
                        renderItem={({item, index}) =>
                            this.renderItem(item, index)
                        }
                    />
                    {!this.props.isFetching && this.props.record.length == 0 &&
                    <PText style={{textAlign: 'center', marginTop: hp("30%"), fontWeight: 'bold'}}
                           text={"No records found"}/>
                    }
                </View>
            </ScrollView>


            {this.props.isFetching &&
            <View>
                <Spinner
                    color={'#3686FF'}
                    visible={true}
                />
            </View>
            }
            </View>
        </Container>
    }
}

const styles = StyleSheet.create({
    bottomBarView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: wp('15%')
    }, searchSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        width: wp("68%"),
        height: hp('5%'),
        paddingRight: 10,
        paddingLeft: 10,
        color: "white",
    },
    iconStyle: {
        fontSize: wp('4%'),
        textAlign: 'center',
        color: "white",
    },
    textStyle: {
        color: 'white',
        fontWeight: 'bold',
    },
    itemMainView: {
        flexDirection: 'row',
        position: 'absolute',
        backgroundColor: 'grey',
        opacity: 0.8,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

function mapStateToProps(state) {
    return {
        record: state.DiscoverReducer.record,
        isFetching: state.DiscoverReducer.isFetching,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchDiscoverRecord
    }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(DiscoverScreen);
