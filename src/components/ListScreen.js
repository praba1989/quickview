import React from 'react';
import {Card, Container, Icon, View} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    heightPercentageToDP as hp,
    removeOrientationListener as rol,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {
    FlatList,
    Image,
    ImageBackground,
    ScrollView,
    StatusBar,
    StyleSheet,
    TextInput,
    TouchableOpacity
} from 'react-native';
import PText from "../utils/PText";
import Colors from "../utils/Colors";
import {Constant} from "../utils/Constant";
import {fetchRequest, fetchTopRatedRecord, search} from "../action/ApiActions";
import Spinner from "react-native-loading-spinner-overlay";

const ISO6391 = require('iso-639-1');

const Header = [
    {
        name: "Top Rated Movies",
        iconName: "",
    }, {
        name: "Upcoming Movies",
        icon: "",
    }, {
        name: "Popular TV Shows",
        icon: "",
    }, {
        name: "Top rated TV Shows",
        icon: "",
    }, {
        name: "Popular People",
        icon: "",
    }
]

class ListScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentVisibleIndex: 0,
            mainBoard: true,
            entryScreen: false,
            serviceScreen: false,
            circleScreen: false,
            slateScreen: false,
            searchActive: false,
            filterSelectedIndex: 0,
            selectedItem: "Top Rated Movies"
        };

    }

    componentDidMount() {

        var api_url = Constant.base_url + "movie/top_rated?api_key=" + Constant.token;
        this.props.fetchTopRatedRecord(api_url);
    }

    componentWillUnmount() {
        rol();
    }


    renderFilterItem(item, index) {

        return (
            <TouchableOpacity onPress={() => {

                this.props.fetchRequest()
                this.setState({
                    filterSelectedIndex: index,
                    selectedItem: item.name
                })
                var title = "movie/top_rated";
                if (item.name == "Upcoming Movies") {
                    title = "movie/upcoming";
                } else if (item.name == "Popular TV Shows") {
                    title = "tv/popular";
                } else if (item.name == "Top rated TV Shows") {
                    title = "tv/top_rated";
                } else if (item.name == "Popular People") {
                    title = "person/popular";
                }
                var api_url = Constant.base_url + title + "?api_key=" + Constant.token;
                this.props.fetchTopRatedRecord(api_url);
            }} style={{
                flexDirection: 'row',
                margin: wp("2%"),
                borderWidth: 1,
                minWidth: wp("15%"),
                borderColor: this.state.filterSelectedIndex == index ? Colors.PrimaryColor : Colors.SecondaryColor,
                borderRadius: wp('1.5%'),
                padding: wp("2%"),
                paddingHorizontal: wp("3%"),
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: this.state.filterSelectedIndex == index ? Colors.PrimaryColor : 'white'
            }}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>

                    <PText style={{
                        color: this.state.filterSelectedIndex == index ? "white" : Colors.SecondaryColor,
                        marginHorizontal: wp("2%")
                    }} text={item.name}/>
                </View>
            </TouchableOpacity>
        )
    }


    renderItem(item, index) {

        var title = item.original_title;
        var overview = item.overview;
        var language = item.original_language;
        var profile = item.backdrop_path;
        var rate = item.vote_average;
        var poster = item.poster_path;
        try {
            let header = this.state.selectedItem;

            if (header == "Popular TV Shows" || header == "Top rated TV Shows") {
                title = item.name;
            } else if (header == "Popular People") {
                title = item.name;
                overview = item.known_for_department;
                overview = item.known_for[0]["overview"];
                rate = item.known_for[0]["vote_average"];
                language = item.known_for[0]["original_language"];
                poster = item.known_for[0]["poster_path"];
                profile = item.profile_path;
            }

        } catch (e) {

        }

        return (
            <Card style={{
                borderRadius: wp("2%"),
                width: wp("98%"),
                height: hp("15%"),
                justifyContent: "center",
                alignItems: "center"
            }}>

                <ImageBackground style={{width: wp("98%"), height: hp("15%"), opacity: 0.5, borderRadius: wp("2%")}}
                                 source={{uri: "http://image.tmdb.org/t/p/w500" + poster}}>
                </ImageBackground>
                <View style={styles.itemMainView}>
                    <View>
                        <Image style={{width: wp("30%"), height: hp("15%"), borderRadius: wp("2%")}}
                               source={{uri: "http://image.tmdb.org/t/p/w500" + profile}}/>
                    </View>
                    <View style={{
                        width: wp("68"),
                        paddingLeft: wp("2%")
                    }}>
                        <PText style={[styles.textStyle, {color: 'black'}]} text={title}/>
                        <PText numberOfLines={2} style={[styles.textStyle, {fontSize: wp("3%")}]} text={overview}/>


                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon style={[styles.iconStyle, {fontSize: wp("2%")}]} type={"FontAwesome"}
                                  name={"language"}/>
                            <PText style={[styles.textStyle, {paddingLeft: wp("2%"), fontSize: wp("2%")}]}
                                   text={ISO6391.getName(language)}/>
                        </View>
                        <PText style={[styles.textStyle, {
                            paddingTop: hp("0%"),
                            textAlign: "right",
                            paddingRight: wp("1%"),
                            fontSize: wp("2.7%"),
                        }]} text={"Ratings " + rate + "/10"}/>

                    </View>
                </View>
            </Card>
        )
    }


    render() {
        return <Container>
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                padding: wp('2%'),
                backgroundColor: Colors.PrimaryColor
            }}>

                <View style={{width: wp("10%"), justifyContent: 'center'}}>
                    <TouchableOpacity onPress={() => {
                        this.props.navigation.openDrawer();
                    }}>
                        <View style={{width: wp("10%")}}>
                            <Icon style={[styles.iconStyle, {color: 'white',}]} type="FontAwesome"
                                  name="bars"/>
                        </View>
                    </TouchableOpacity>

                </View>
                <View style={{
                    //width: wp("58%"),
                    flexDirection: 'row',
                }}>
                    <View style={styles.searchSection}>
                        <TextInput
                            ref={ref => this.textInputRef = ref}
                            placeholderTextColor={"white"}
                            style={[styles.input, {
                                borderBottomWidth: this.state.searchActive ? 1 : 0,
                                borderColor: 'white'
                            }]}
                            placeholder={this.state.searchActive ? "Search by name" : ""}
                            onChangeText={(searchString) => {

                                this.props.search(searchString, this.state.selectedItem);
                            }}
                            underlineColorAndroid="transparent"
                        />
                    </View>
                </View>
                <View style={{width: wp("33%"), flexDirection: 'row'}}>
                    <TouchableOpacity onPress={() => {
                        this.setState({
                            searchActive: true
                        })
                        this.textInputRef.focus()
                    }
                    }>
                        <View style={{width: wp("11%")}}>
                            <Icon style={[styles.iconStyle, {color: 'white', fontSize: wp("6%")}]} type="Feather"
                                  name="search"/>
                        </View>
                    </TouchableOpacity>
                    <Icon style={[styles.iconStyle, {color: 'white', fontSize: wp("7%")}]} type="MaterialIcons"
                          name="notifications-none"/>
                </View>

            </View>

            <View>
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    horizontal
                    data={Header}
                    renderItem={({item, index}) =>
                        this.renderFilterItem(item, index)
                    }
                />
            </View>

            <ScrollView style={{minHeight: hp("80%")}}>
                <View>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.props.record}
                        renderItem={({item, index}) =>
                            this.renderItem(item, index)
                        }
                    />
                    {!this.props.isFetching && this.props.record.length == 0 &&
                    <PText style={{textAlign: 'center', marginTop: hp("30%"), fontWeight: 'bold'}}
                           text={"No records found"}/>
                    }
                </View>
            </ScrollView>


            {this.props.isFetching &&
            <View>
                <Spinner
                    color={'#3686FF'}
                    visible={true}
                />
            </View>
            }
        </Container>
    }
}

const styles = StyleSheet.create({
    bottomBarView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: wp('15%')
    }, searchSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        width: wp("68%"),
        height: hp('5%'),
        paddingRight: 10,
        paddingLeft: 10,
        color: "white",
    },
    iconStyle: {
        fontSize: wp('4%'),
        textAlign: 'center',
        color: "white",
    },
    topBoxView: {
        borderColor: Colors.SecondaryColor,
        borderWidth: 0,
        flexDirection: 'row',
        borderBottomWidth: 0,
        padding: wp('1%')
    },
    bottomBoxView: {
        flexDirection: 'row',
        justifyContent: 'center',
        width: wp('20%'),
        alignItems: 'center'
    },
    textStyle: {
        color: 'white',
        fontWeight: 'bold',
    },
    itemMainView: {
        flexDirection: 'row',
        position: 'absolute',
        backgroundColor: 'grey',
        opacity: 0.8,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

function mapStateToProps(state) {
    return {
        record: state.dashboardReducer.record,
        isFetching: state.dashboardReducer.isFetching,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchTopRatedRecord,
        fetchRequest,
        search
    }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ListScreen);
