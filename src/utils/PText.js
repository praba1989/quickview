import React, {Component} from 'react';
import {Platform, StyleSheet, Text} from 'react-native';
import Colors from "./Colors";

export default class PText extends Component {
    render() {
        return (
            <Text {...this.props} numberOfLines={this.props.numberOfLines}
                  style={[Platform.OS == 'android' && styles.AppText, this.props.style]}>{this.props.text}</Text>
        )
    }
}
const styles = StyleSheet.create({
    AppText: {
        fontFamily: '',
        color: Colors.TextColor
    },
});
