import React from 'react';
import {Icon} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {widthPercentageToDP as wp,} from 'react-native-responsive-screen';
import {StatusBar, StyleSheet, TouchableOpacity, View,} from 'react-native';
import Colors from "./Colors";
import PText from "./PText";

class TopToolBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return <View style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 1,
            height: 50,
            backgroundColor: Colors.TopToolbarColor,
        }}>
            <PText style={[styles.titleStyle, this.props.style]} text={this.props.title}/>
        </View>;
    }
}

const styles = StyleSheet.create({
    iconStyle: {
        fontSize: wp('6%'),
        color: 'white',
        paddingLeft: wp('4%'),
        textAlign: 'center'
    },
    titleStyle: {
        fontSize: wp('4.5%'),
        color: 'white',
        textAlign: 'center',
        paddingLeft: wp('4%')
    },
});

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({});
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(TopToolBar);
