export default {
    PrimaryColor: '#886ab5',
    TopToolbarColor: '#886ab5',
    SecondaryColor: '#868e96',
    TextColor: "#212529",
    Success: '#1dc9b7',
    Warning: '#ffc241',
    Error: '#FD3995',
    Info: '#2196f3',
    CheckedColor: "#00D3B3",
    LineColor: "##D2D2D2",
};
