import {
    FETCH_RECORD_SEARCH,
    FETCH_TOPRATEDRECORD_FAILURE,
    FETCH_TOPRATEDRECORD_REQUEST,
    FETCH_TOPRATEDRECORD_SUCCESS
} from '../Actiontypes';

const initialState = {
    isFetching: true,
    msg: '',
    record: [],
    allRecord: [],
};

const listReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TOPRATEDRECORD_REQUEST:
            return {...state, isFetching: true, record: []};

        case FETCH_TOPRATEDRECORD_FAILURE:
            return {...state, msg: action.data, record: [], isFetching: false};

        case FETCH_TOPRATEDRECORD_SUCCESS:
            return {
                ...state,
                record: action.payload,
                allRecord: action.payload,
                isFetching: false,
            };
        case FETCH_RECORD_SEARCH:

            var filteredList = state.allRecord;
            var searchType = action.searchType;

            var searchKey = "name";
            if (searchType == "Top Rated Movies" || searchType == "Upcoming Movies") {
                searchKey = "original_title"
            }
            if (action.search != "") {
                let text = action.search.toLowerCase()
                let oldData = state.allRecord
                filteredList = oldData.filter((item) => {

                    if (item[searchKey].toLowerCase().match(text)) {
                        return item;
                    }

                })
            }

            return {
                ...state,
                record: filteredList,
                isFetching: false,
            };

        default:
            return state;
    }
};

export default listReducer;
