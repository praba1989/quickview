import {
    FETCH_DISCOVER_FAILURE,
    FETCH_DISCOVER_REQUEST,
    FETCH_DISCOVER_SUCCESS
} from '../Actiontypes';

const initialState = {
    isFetching: true,
    msg: '',
    record: [],
};

const discoverReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DISCOVER_REQUEST:
            return {...state, isFetching: true, record: []};

        case FETCH_DISCOVER_FAILURE:
            return {...state, msg: action.data, record: [], isFetching: false};

        case FETCH_DISCOVER_SUCCESS:
            return {
                ...state,
                record: action.payload,
                isFetching: false,
            };

        default:
            return state;
    }
};

export default discoverReducer;
