import {
    FETCH_TRENDING_FAILURE,
    FETCH_TRENDING_REQUEST,
    FETCH_TRENDING_SUCCESS
} from '../Actiontypes';

const initialState = {
    isFetching: true,
    msg: '',
    record: [],
};

const listReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRENDING_REQUEST:
            return {...state, isFetching: true, record: []};

        case FETCH_TRENDING_FAILURE:
            return {...state, msg: action.data, record: [], isFetching: false};

        case FETCH_TRENDING_SUCCESS:
            return {
                ...state,
                record: action.payload,
                isFetching: false,
            };

        default:
            return state;
    }
};

export default listReducer;
