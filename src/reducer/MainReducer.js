import {combineReducers} from 'redux';
import listReducer from './ListReducer'
import TrendingReducer from './TrendingReducer'
import DiscoverReducer from './DiscoverReducer'

const rootReducer = combineReducers({
    dashboardReducer:listReducer,
    trendingReducer:TrendingReducer,
    DiscoverReducer:DiscoverReducer
});

export default rootReducer;
